# Reto-DevOps 
## CLM

Pasos previos: 
* git clone https://gitlab.com/clm-public/reto-devops.git
* Instalar dependencias 
    - npm install 
* Ejecutar Test 
    - npm run test 
        > basicservice@1.0.0 test /basic-unit-test
        > jest
* Ejecutar la app 
    - node index.js 
    - acceder a la API localmente en el puerto 3000.
        >   curl -s localhost:3000/ | jq

        "msg": "ApiRest prueba"

        > curl -s localhost:3000/public | jq

        "public_token": "12837asd98a7sasd97a9sd7"

        > curl -s localhost:3000/private | jq

        "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="

  
# Reto 1. Dockerizar la aplicación

![alt text](app/img/nodedoker.jpg "Title Text"){width=50%}

Usé Docker para contener la app, para generar un entorno consistente que incluya todo lo requerido para ejecutar su servicio. 

![alt text](app/img/dockerfile.png "Title Text")
# 1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)

Seleccioné la imagen de node:14.5.0-alpine como su base, ya que Alpine Linux es una distribucion de Linux **pequeña**, **liviana** y **compatible**; ademas elimina muchos archivos programas, dejando solo lo suficiente para ejecutar la aplicacion, es decir ideal para produccion. 

# 2. Ejecutar la app como un usuario diferente de root.

De forma predeterminada, la imagen Node de Docker incluye un usuario node no root que puede usar para evitar ejecutar el contenedor de su aplicación como root. Evitar ejecutar contenedores como root y restringir las capacidades del contenedorsolo a las necesarias para la ejecución de sus procesos es una práctica de seguridad recomendada. 

# Reto 2. Docker Compose

![alt text](app/img/docker-compose.png "Title Text")

# 1. Nginx que funcione como proxy reverso a nuesta app Nodejs

