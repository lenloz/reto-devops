FROM node:alpine

ENV PORT 3000

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY index.js ./ 

EXPOSE ${PORT}

USER node

CMD ["node", "index.js"]




